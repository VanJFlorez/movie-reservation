class ReservationsController < ApplicationController

  #GET /movies/:movie_id/reservations
  def index
    @movie = Movie.find(params[:movie_id])
    @movie_reservations = @movie.reservations

    render json: @movie_reservations
  end

  #GET /movies/:movie_id/reservations/:id
  def show
    @reservation = Reservation.where(movie_id: params[:movie_id], id: params[:id])
    render json: @reservation
  end

  #GET /reservations
  def show_all
    @reservations = Reservation.all
    render json: @reservations
  end

  #POST /movies/:movie_id/reservations
  # Here we have the limited seats restriction
  def create
    @movie = Movie.find(params[:movie_id])
    if @movie.n_seats > 0
      @reservation = @movie.reservations.create(reservation_params)
      @movie.n_seats = @movie.n_seats - 1
      @movie.save
      render json: @reservation
    else
      render json: {"title": "info", "Message": "No seats available, limit reached."}
    end
      
  end
  
  private
      # Use callbacks to share common setup or constraints between actions.
      def set_reservation
          @reservation = Reservation.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def reservation_params
          params.require(:reservation).permit(:citz_id, :email, :name, :phone_num)
      end

end
