Rails.application.routes.draw do
  resources :movies do
    resources :reservations
  end

  get "/movies/by_date/:year/:month/:day",  to: "movies#show_by_date"
  get "/reservations", to: "reservations#show_all"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
