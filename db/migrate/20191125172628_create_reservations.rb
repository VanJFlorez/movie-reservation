class CreateReservations < ActiveRecord::Migration[6.0]
  def change
    create_table :reservations do |t|
      t.string :citz_id
      t.string :name
      t.string :phone_num
      t.string :email
      t.references :movie, null: false, foreign_key: true

      t.timestamps
    end
  end
end
