class CreateMovies < ActiveRecord::Migration[6.0]
  def change
    create_table :movies do |t|
      t.string :name
      t.string :descr
      t.string :url_img
      t.date :date
      t.time :time
      t.integer :n_seats

      t.timestamps
    end
  end
end
