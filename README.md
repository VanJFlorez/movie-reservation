# REST API Samples
# GET
- localhost:3000/movies                       # all movies
- localhost:3000/movies/1/reservations/4      # detail reservation
- localhost:3000/movies/2                     # detail movie
- localhost:3000/movies/by_date/2019/11/1     # all movies by day
- localhost:3000/reservations                 # all reservations

# POST
- localhost:3000/movies/1/reservations        # do a movie reservation = create a reservation into a movie
>Sample data
```json
{
	"citz_id": "234234234", 
	"email": "email6",
	"name": "name66",
	"phone_num": "345345345"
}
```

- localhost:3000/movies                       # create a movie
>Sample data
```json
{
	"name": "name3",
	"descr": "descr3",
	"url_img": "url3",
	"date": "2019-11-01",
	"time": "14:33:00",
	"n_seats": "10"
}
```
